package com.gitlab.marioluciano.kotlinsemanticversioning

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinSemanticVersioningApplication

fun main(args: Array<String>) {
	runApplication<KotlinSemanticVersioningApplication>(*args)
}
